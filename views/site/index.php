<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Ejemplo 1 - Yii 2';

?>
<div class="container">
    <div class="row row-flex row-flex-wrap">
        <?php
        $div = '<div class="clearfix"></div>';
        foreach ($noticias as $k => $noticia) {
            echo $this->render("_noticias", [
                "titulo" => $noticia->titulo,
                "texto" => $noticia->texto,
                
            ]);
        }
        ?>
    </div>
</div>
