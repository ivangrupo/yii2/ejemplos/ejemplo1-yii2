<div class="col-sm-12 col-md-6 p-2 bg-danger">
    <div class="thumbnail">
      <div class="caption">
        <h3 class="alert alert-danger"><?= $titulo; ?></h3>
        <p><?= $texto; ?></p>
      </div>
    </div>
 </div>

