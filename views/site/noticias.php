<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'titulo',
            'texto',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>

