<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Noticias;
use yii\data\ActiveDataProvider;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        $consulta = Noticias::find();
        $salidaArrayObjetos=$consulta->all();
 
        return $this->render('index',[
            'noticias'=>$salidaArrayObjetos,
        ]);
        
    }
    
    public function actionNoticias(){
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);
        
        return $this->render('noticias', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionNoticias1(){
        $salida = Noticias::find()->all();
        return $this->render('noticias', [
            'datos' => $salida,
        ]);
    }
    
    public function actionNoticias2(){
        $salida = Noticias::find()
                ->where(["titulo"=>0])
                ->all();
        return $this->render('noticias', [
            'datos' => $salida,
        ]);
    }

}

